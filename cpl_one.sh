#!/bin/bash

#-----------------------------------------------------------------------------
# The function get_intel_version
#
# Input:
#
# Output:
#    RET_1    Stripped value
#
#-----------------------------------------------------------------------------
# function get_intel_version
# {
#     #---  Get first line of stderr for command "icl /QV"
#     #for /F "tokens=* USEBACKQ" %%F in (`icl /QV 2^>^&1`) do ( 
#     #    _VER=%%F 
#     #    goto end_for_intel_version
#     #)
#     #:end_for_intel_version
#     
#     #---  Filter main version number
#     #_VER=$_VER:*Version =%
#     #_END=$_VER:*.=%
#     #_VER=$_VER:%_END%=%%
#     #_VER=%_VER:.=%
# 
#     _VER="get_intel_version: unset"
#     RET_1=$_VER
# }

#-----------------------------------------------------------------------------
# main()
#-----------------------------------------------------------------------------
function main
{
    # ---  Compile b2 if necessary
    if [ ! -x "./b2" ]; then
        ./bootstrap.sh
    fi

    # ---  Get Intel version
    # get_intel_version && _INTL="RET_1"
    # if %errorLevel% NEQ 0 (exit /b )

    # ---  Target libs
    TGTS=""
    TGTS+=" --with-system"
    TGTS+=" --with-filesystem"
    TGTS+=" --with-thread"

    # ---  Options
    OPTS=
    OPTS+=" address-model=64"
    OPTS+=" variant=release"
    #OPTS+=" toolset=intel-%_INTL%-vc%_MSVC%"
    OPTS+=" toolset=gcc"
    OPTS+=" link=static"
    OPTS+=" runtime-link=shared"

    # ---  Compile
    rm -rf bin.v2 > /dev/null
    ./b2 --stagedir=./_run/unx64/release --layout=system $TGTS $OPTS variant=release stage
    # del /q /s /f bin.v2 > NUL
    # ./b2 --stagedir=./_run/unx64/debug   --layout=system $TGTS $OPTS variant=debug   stage
    # del /q /s /f bin.v2 > NUL
}

main

